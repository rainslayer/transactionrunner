﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OperationsList = System.Collections.Generic.LinkedList<Interdimensional.TransactionRunner.OperationType>;
using OperationsResult = System.Collections.Generic.Dictionary<string, object?>;

namespace Interdimensional.TransactionRunner;

public class OperationType
{
  public required string OperationName { get; set; }
  public required Delegate Operation { get; set; }
  public Delegate? Rollback { get; set; }
}

public enum RunnerState
{
  RolledBack = -1,
  Initialized = 0,
  Commited = 1
};

public class TransactionRunner
{
  private readonly OperationsList _operations = new OperationsList();
  private readonly OperationsResult _results = new OperationsResult();
  private RunnerState _state = RunnerState.Initialized;
  public RunnerState State => _state;
    
  public TransactionRunner Add<TResult>(string name, Func<OperationsResult, TResult> operation, Action<OperationsResult>? rollback = null)
  {
    var op = new OperationType { OperationName = name, Operation = operation, Rollback = rollback };
    AppendOperation(op);

    return this;
  }      
  
  public TransactionRunner Add<TResult>(string name, Func<OperationsResult, Task<TResult>> operation, Action<OperationsResult>? rollback = null)
  {
    var op = new OperationType { OperationName = name, Operation = operation, Rollback = rollback };
    AppendOperation(op);

    return this;
  }      
  
  public TransactionRunner Add(string name, Action<OperationsResult> operation, Action<OperationsResult>? rollback = null)
  {
    var op = new OperationType { OperationName = name, Operation = operation, Rollback = rollback };
    AppendOperation(op);

    return this;
  }    
  
  public TransactionRunner Add<TResult>(string name, Func<TResult> operation, Action<OperationsResult>? rollback = null)
  {
    var op = new OperationType { OperationName = name, Operation = operation, Rollback = rollback };
    AppendOperation(op);

    return this;
  }
    
  public ExecutionType<T> Returns<T>(string operationName)
  {
    return new ExecutionType<T>(this, operationName);
  }

  public void Execute()
  {
    new ExecutionType(this).Execute();
  }

  public Task ExecuteAsync()
  {
    return new ExecutionType(this).ExecuteAsync();
  }

  private void ExecuteOperations()
  {
    if (_state is not RunnerState.Initialized)
    {
      throw new ApplicationException("Runner can't be called more than once");
    }

    var currentNode = _operations.First;
    while (currentNode is not null)
    {
      var operation = currentNode.Value;
      try
      {
        Executor(operation.OperationName, operation.Operation);
        currentNode = currentNode.Next;
      }
      catch (Exception e)
      {
        Rollback(currentNode);
        throw new Exception($"Transaction have failed on operation \"{operation.OperationName}\" and was rolled back",
          e);
      }
    }
  }

  private async Task ExecuteOperationsAsync()
  {
    if (_state is not RunnerState.Initialized)
    {
      throw new ApplicationException("Runner can't be called more than once");
    }

    var currentNode = _operations.First;
    while (currentNode is not null)
    {
      var operation = currentNode.Value;

      try
      {
        await AsyncExecutor(operation.OperationName, operation.Operation);
        currentNode = currentNode.Next;
      }
      catch (Exception e)
      {
        await RollbackAsync(currentNode);
        throw new Exception($"Transaction have failed on operation \"{operation.OperationName}\" and was rolled back",
          e);
      }
    }
  }

  private void Executor(string operationName, Delegate operation)
  {
    if (operation is Func<OperationsResult> or Action<OperationsResult>)
    {
      _results[operationName] = operation.DynamicInvoke(_results) ?? null;
    }
    else
    {
      _results[operationName] = operation.DynamicInvoke() ?? null;
    }
  }

  private async Task AsyncExecutor(string operationName, Delegate operation)
  {
    if (operation is Func<OperationsResult> or Action<OperationsResult>)
    {
      _results[operationName] = operation.DynamicInvoke(_results) ?? null;
    }
    else if (operation.Method.ReturnType.IsSubclassOf(typeof(Task)))
    {
      dynamic invoke = operation is Func<OperationsResult, Task>
        ? operation.DynamicInvoke(_results)
        : operation.DynamicInvoke();

      if (invoke is not null)
      {
        _results[operationName] = await invoke ?? null;
      }
    }
    else
    {
      _results[operationName] = operation.DynamicInvoke() ?? null;
    }
  }

  private void Rollback(LinkedListNode<OperationType>? currentNode)
  {
    while (currentNode is not null)
    {
      var operation = currentNode.Value;
      try
      {
        if (operation.Rollback is not null)
        {
          Executor(operation.OperationName, operation.Rollback);
        }

        currentNode = currentNode.Previous;
      }
      catch
      {
        throw new Exception($"Error rolling back operation {operation.OperationName}");
      }
    }

    _state = RunnerState.RolledBack;
  }

  private async Task RollbackAsync(LinkedListNode<OperationType>? currentNode)
  {
    while (currentNode is not null)
    {
      var operation = currentNode.Value;

      try
      {
        if (operation.Rollback is not null)
        {
          await AsyncExecutor(operation.OperationName, operation.Rollback);
        }

        currentNode = currentNode.Previous;
      }
      catch
      {
        throw new Exception($"Error rolling back operation {operation.OperationName}");
      }
    }

    _state = RunnerState.RolledBack;
  }

  private void AppendOperation(OperationType operation)
  {
    _operations.AddLast(operation);
  }

  public class ExecutionType
  {
    private readonly TransactionRunner _parent;

    internal ExecutionType(TransactionRunner parent)
    {
      _parent = parent;
    }

    public void Execute() => _parent.ExecuteOperations();
    public async Task ExecuteAsync() => await _parent.ExecuteOperationsAsync();
  }

  public class ExecutionType<T>
  {
    private readonly TransactionRunner _parent;
    private readonly string _returnKey;

    internal ExecutionType(TransactionRunner parent, string returnKey)
    {
      _parent = parent;
      _returnKey = returnKey;
    }

    public T? Execute()
    {
      _parent.ExecuteOperations();
      return (T?)_parent._results[_returnKey];
    }

    public async Task<T?> ExecuteAsync()
    {
      await _parent.ExecuteOperationsAsync();
      return (T?)_parent._results[_returnKey];
    }
  }
}

public static class DictionaryExtensions
{
  public static object? GetResult(this Dictionary<string, object?> instance, string name)
  {
    var value = instance[name];

    if (value is not null)
    {
      return value;
    }

    return null;
  }

  public static T? GetResult<T>(this Dictionary<string, object?> instance, string name)
  {
    try
    {
      return (T?)GetResult(instance, name);
    }
    catch
    {
      throw new ApplicationException("Resulting value is not available");
    }
  }
}